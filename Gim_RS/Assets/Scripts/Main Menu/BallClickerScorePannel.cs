﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallClickerScorePannel : MonoBehaviour{
    const string HighScoreFormat = "{0} - {1}:\t\t{2}";
    [SerializeField]
    List<Text> HighScoreEntries = new List<Text>();

    void Start()
    {
        PopulateHighScores();
    }

    void PopulateHighScores()
    {
     var scores =   BallClickerHighScores.GetEntries();
        // wyniki sa posortowanae

        int savedScores = scores.Count;
        for (int i = 0; i < HighScoreEntries.Count; i++)
        {
            if (i<savedScores)
            {
                HighScoreEntries[i].text = string.Format(HighScoreFormat, i + 1, scores[i].PlayerName, scores[i].Score);
            }
            else
            {
                HighScoreEntries[i].text = "";
            }
        }

    }
    public void ResetHighScores()
    {
        BallClickerHighScores.DeleteScores();
        PopulateHighScores();
    }
}



