﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PoolableObject : MonoBehaviour {

    public Action OnHideInPool = delegate() { };
    public Action OnBringFromPool = delegate () { };

    [HideInInspector]
    public PoolableObject SourcePrefab;

   void Awake()
    {
        OnHideInPool += Hide;
        OnBringFromPool += Show;
    }

    void Hide()
    {
        gameObject.SetActive(false);
    }

    void Show()
    {
        gameObject.SetActive(true);
    }

//     void Reset()
//    {
//#if UNITY_EDITOR
//        //trzeba pamietac , aby nie tworzyc pol przy lompilacji warunkkowej , szczeegolnie sprawdzajax czy kod dzia;a w edytorze

//        //this code desnt work
//        var prefabType = UnityEditor.PrefabUtility.GetPrefabType(gameObject);
//        if (prefabType == UnityEditor.PrefabType.Prefab)
//        {
//            SourcePrefab = this;
//        }
        
//#endif
    //}
    public void PushToPool()
    {//przyda sie przy [articlach
        ObjectPool.PushIntoPool(this);

    }
}
