﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class ObjectPool
{
    public static bool AutoClearPoolOnSceneChange = true;
    static Dictionary<PoolableObject, Stack<PoolableObject>> Pools = new Dictionary<PoolableObject, Stack<PoolableObject>>();

    static ObjectPool()
    {
        SceneManager.activeSceneChanged += OnSceneChanged;
    }
    /// <summary>
    /// get poolable object from pull and calls OnBringFroomPool event
    /// If it returns null then instance object yourself
    ///</summary>

    static public PoolableObject GetFromPool(PoolableObject prefab)
    {
        PoolableObject pooledObject = null;
        if (Pools.ContainsKey(prefab))
        {
            var stack = Pools[prefab];
            if (stack.Count > 0)
            {
                do
                {
                    // ta petla jest potrzebna, poniewaz obiekt mogl byc zniszczony niezaleznie od dzialania puli
                    pooledObject = stack.Pop();
                }
                while ((pooledObject == null) && (stack.Count > 0));

                if (pooledObject != null)
                {
                    pooledObject.OnBringFromPool();
                }

            }
        }

        if (pooledObject == null)
        {
            pooledObject = Object.Instantiate<PoolableObject>(prefab);
            // this fix isue with PoolableObjectReset method
            pooledObject.SourcePrefab = prefab;

        }

        return pooledObject;
    }

    static public void PushIntoPool(PoolableObject poolable)
    {
        if (poolable == null || poolable.SourcePrefab == null)
        {
            Debug.LogWarning("Poolable Object to cache in pool cannot be null and it's source prefab reference cannot be null");
            return;
        }

        if (Pools.ContainsKey(poolable.SourcePrefab))
        {
           if(Pools[poolable.SourcePrefab].Contains(poolable))
            {
                Debug.LogWarning("Don't Push an object twice to pool");
                return;
            }
            Pools[poolable.SourcePrefab].Push(poolable);
        }
        else
        {
            var stack = new Stack<PoolableObject>();
            stack.Push(poolable);
            Pools.Add(poolable.SourcePrefab, stack);
        }

        poolable.OnHideInPool.Invoke();
    }

    static void OnSceneChanged(Scene from, Scene to)
    {
        if (AutoClearPoolOnSceneChange)
        {
            Pools.Clear();
        }
    }
}
