﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PoolableObject))]
[RequireComponent(typeof(ParticleSystem))]

public class PoolableParticle : MonoBehaviour {
    ParticleSystem m_Particle;
    PoolableObject m_Poolable;

     void Awake()
    {
        m_Particle = GetComponent<ParticleSystem>();
        m_Poolable = GetComponent<PoolableObject>();

        m_Poolable.OnBringFromPool += () =>
        {
            m_Particle.Play();
        };

        m_Poolable.OnHideInPool += () =>
        {
            m_Particle.Stop();
        };
    }

    public void OnParticleSystemStopped()
    {
        m_Poolable.PushToPool();
    }

}
