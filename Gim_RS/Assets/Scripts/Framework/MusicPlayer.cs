﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    #region Component

    [SerializeField]
    AudioSource m_Speaker;
    #endregion
    #region Static Api

    static MusicPlayer s_Instance;

    [RuntimeInitializeOnLoadMethod]
    static void InstantiatePlayer()
    {
        Debug.Log("Creating music Player----");
        s_Instance = Instantiate<MusicPlayer>( Resources.Load<MusicPlayer>("Music/MusicPlayer"));
        DontDestroyOnLoad(s_Instance.gameObject);
        Debug.Log("Music Player Cretaed");
    }

    public static void SetMusic(AudioClip musicClip)
    {
        s_Instance.m_Speaker.clip = musicClip;
        s_Instance.m_Speaker.loop = true;
        s_Instance.m_Speaker.Play();
    }
    #endregion
}
