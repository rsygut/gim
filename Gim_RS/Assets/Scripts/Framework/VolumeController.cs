﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class VolumeController : MonoBehaviour {

    public AudioMixer Mixer;
    public string ParametrName;
    public Slider Slider;

    void OnEnable()
    {
        float dbVolume = 0.0f;
        Mixer.GetFloat(ParametrName, out dbVolume);
        float linearVolume = GetLinear(dbVolume);
        Debug.Log("ser slider to " + linearVolume + "db volume" + dbVolume);
        Slider.value = linearVolume;
        Slider.onValueChanged.AddListener(SetVolume);

        for (float i = 0.0f; i <= 1.0f; i+=0.1f)
        {
            if (Mathf.Approximately(i, GetLinear(GetDB(i)))) 
            {
                Debug.LogFormat("Dla wartosci {0} konwersja danych działa", i);
            }
            else
            {
                Debug.LogErrorFormat("Dla wartosci {0} w konwersji jest błąd , DB = {1}; Linear DB = {2}", i, GetDB(i), GetLinear(GetDB(i)));
    }
        }
    }

    void OnDisable()
    {
        Slider.onValueChanged.RemoveListener(SetVolume);
    }




    void SetVolume(float volume)
    {
        float dbVolume = GetDB(volume);
        Debug.LogFormat("Ustawiam głoścno {0} na {1}", ParametrName, dbVolume);
        Mixer.SetFloat(ParametrName, dbVolume);
    }



    float GetDB(float linear)
    {
        return Mathf.Lerp(-80.0f, 0.0f, Mathf.Pow(linear, 0.25f));
    }

    float GetLinear(float db)
    {
        float t = Mathf.InverseLerp(-80.0f, 0.0f, db);
        return Mathf.Pow(t, 4.0f);
    }

}
