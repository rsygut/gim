﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SfxPlayer : MonoBehaviour
{
    [SerializeField]
    private List<EventAudioClipPair> m_Pairs = new List<EventAudioClipPair>(); 


    AudioSource m_Speaker;
    AudioSource Speaker
    {
        get
        {
            if (m_Speaker==null)
            {
                m_Speaker = GetComponent<AudioSource>();
            }

            return m_Speaker;
        }
    }

    void Awake()
    {
        foreach (var pair in m_Pairs)
        {
            System.Action playSoundAction = () =>
            {
                Speaker.PlayOneShot(pair.Clip);
            };

            pair.Event.Action += playSoundAction;
            pair.EventCache += playSoundAction;
        }
    }

    void OnDestroy()
    {
        foreach (var pair in m_Pairs)
        {
            pair.Event.Action -= pair.EventCache;
        }
    }

    [System.Serializable]
    class EventAudioClipPair
    {
        public SimpleEvent Event;
        public AudioClip Clip;

        ///<summary>
        ///Used to clean reference directory
        ///</summary>
        public System.Action EventCache;
    }

}