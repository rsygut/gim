﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SimpleEvent", menuName = "Gameplay Events/Simple Event")]
public class SimpleEvent : ScriptableObject
{
    public System.Action Action = delegate () { };
}

