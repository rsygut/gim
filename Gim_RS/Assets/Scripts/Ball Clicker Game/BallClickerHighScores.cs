﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

[System.Serializable]
public class HighScoreEntry
{
    public string PlayerName = "";
    public int Score = 0;
}
[System.Serializable]
public class BallClickerHighScores
{
    const string HighScoreJSONKey = "BallClicker_HighScores";
    public const int MaxEntries = 10;

    [SerializeField]
    List<HighScoreEntry> Scores = new List<HighScoreEntry>();

    public static List<HighScoreEntry> GetEntries()
    {
        string highScoreJSON = PlayerPrefs.GetString(HighScoreJSONKey, "");
        BallClickerHighScores scores = new BallClickerHighScores();
        JsonUtility.FromJsonOverwrite(highScoreJSON, scores);

        return scores.Scores;


    }

    // unsorted list, to be save on scoerre param name - entries

    public static void SaveEntries(List<HighScoreEntry> entries)
    {
        // sortowanie wynikow
        List<HighScoreEntry> scores = new List<HighScoreEntry>(entries);
        scores.Sort(( score1, score2) => { return (-1) * (score1.Score - score2.Score); });
        if (scores.Count > MaxEntries)
        {
            scores.RemoveRange(MaxEntries - 1, scores.Count - MaxEntries);

        }
        // 2 utworz obiekt
        BallClickerHighScores handler = new BallClickerHighScores();
        handler.Scores = scores;
        // 3 zapisz obiekt
        string scoresJSON = JsonUtility.ToJson(handler);
        PlayerPrefs.SetString(HighScoreJSONKey, scoresJSON);
    }

    public static void DeleteScores()
    {
        PlayerPrefs.DeleteKey(HighScoreJSONKey);
    }
}