﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {
    float m_LastTimeSpawned = 0.0f;
    int m_SpawnCount = 0;

    public int SpawnCount
    {
        get
        {
            return m_SpawnCount;
        }
    }

    public float TimeSinceLastSpawn
    {
        get
        {
            return Time.realtimeSinceStartup - m_LastTimeSpawned;
        }
    }

    public T Spawn<T>( T prefab) where T : MonoBehaviour
    {
        T spawnedObject = UntracketSpawn(prefab, transform.position, transform.rotation);

        m_SpawnCount++;
        m_LastTimeSpawned = Time.realtimeSinceStartup;

        
        return spawnedObject;
    }

    static public T UntracketSpawn<T>( T prefab, Vector3 position,Quaternion rotation) where T:Component
    {// to sie przyda przy spawnowaniu particli
        PoolableObject poolablePrefab = prefab.GetComponent<PoolableObject>();
        T spawnedObject = null;

        if (poolablePrefab != null)
        {
            PoolableObject poolableSpawned = null;
            poolableSpawned = ObjectPool.GetFromPool(poolablePrefab);

            poolableSpawned.transform.position = position;
            poolableSpawned.transform.rotation = rotation;
            //if (poolableSpawned == null)
            //{
            // calosc do usuniecia chyba

            //    poolableSpawned = Instantiate(poolablePrefab, position, rotation);
            //    // this fix isue with PoolableObjectReset method
            //    poolableSpawned.SourcePrefab = poolablePrefab;

            //}
            //else
            //{
            //    //
            //    //ususn
            //}
            spawnedObject = poolableSpawned.GetComponent<T>();
        }
        else
        {
            spawnedObject = Instantiate<T>(prefab, position, rotation);
        }
        return spawnedObject;

    }

     void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, transform.lossyScale);
    }
}
