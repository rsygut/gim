﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallClickerUI : MonoBehaviour {
    const string InGameScoreCounterTextFormat = "SCORE: {0}";
    const string EndScoreTextFormat = "YOUR SCORE: {0}";
    
    [Header("Gameplay Events")]
    public SimpleEvent OnAddScore;
    public SimpleEvent OnFail;

    [Header("UI Elements ")]
    public Text InGameScoreCounter;
    public Text EndScore;

    public GameObject GameOverPanel;
    public GameObject AddHighScorePanel;
    public InputField PlayerNameInputField;
    int m_Score;

    void Awake()
    {
        m_Score = 0;
        OnAddScore.Action += AddScore;
        OnFail.Action += OnGameOver;
        GameOverPanel.SetActive(false);
        AddHighScorePanel.SetActive(false);
        InGameScoreCounter.text = string.Format(InGameScoreCounterTextFormat, m_Score);

    }

    void AddScore() 
    {
        m_Score++;
        InGameScoreCounter.text = string.Format(InGameScoreCounterTextFormat, m_Score);
    }

    void OnGameOver()
    {
        OnFail.Action -= OnGameOver;
        var entries = BallClickerHighScores.GetEntries();
        if (entries.Count<BallClickerHighScores.MaxEntries || entries[entries.Count-1].Score< m_Score)
        {
            ShowAddHighScore();
   
        }
        else
        {
            GameOverPanel.SetActive(false);
        }

        EndScore.text = string.Format(EndScoreTextFormat, m_Score);
        GameOverPanel.SetActive(true);

    }
    public void SaveHighScore()
    {

        var entries = BallClickerHighScores.GetEntries();
        HighScoreEntry entry = new HighScoreEntry();
        entry.Score = m_Score;
        entry.PlayerName = (string.IsNullOrEmpty(PlayerNameInputField.text))?"Anonim":PlayerNameInputField.text;
        entries.Add(entry);
        BallClickerHighScores.SaveEntries(entries);

        AddHighScorePanel.SetActive(false);


    }

    void ShowAddHighScore()
    {
        AddHighScorePanel.SetActive(true);
    }
     void OnDestroy()
    {
        OnAddScore.Action -= AddScore;
        OnFail.Action -= OnGameOver;

    }
}
