﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//TODO rozdzielic skrypt na namespaces, a jeszcze lepiej na assembly definition

//TODO  Spawnowanie particli oprzec o Object Pooling

/// <summary>
/// Podstawowy element gameplayu w grze o klikaniu kulek
/// kulka bedzie spawnowana przez osobny manager,
/// nastepnie bedzie rosnac, a na koncu pekac
/// Jezeli gracz kliknie kulke, zanim ona peknie,
/// to dostanie punkty a ona peknie z innymi particlami
/// </summary>
[RequireComponent(typeof(PoolableObject))]
public class BallToClick : MonoBehaviour
{
    const float LifeTime = 5.0f;
    const float MinSize = 0.01f;
    const float MaxSize = 4.0f;

    float m_LifeTime = 0.0f;

    [Header("Event effects")]
    public ParticleSystem SuccesParticle;
    public ParticleSystem FailParticle;
    

    [Header("Gameplay Events")]
    public SimpleEvent OnAddScore;
    public SimpleEvent OnFail;
    
    
    PoolableObject m_Poolable;
    PoolableObject Poolable
    {
        get
        {
            if (m_Poolable == null)
            {
                m_Poolable = GetComponent<PoolableObject>();
            }

            return m_Poolable;
        }
    }
    // Use this for initialization
    void Awake()
    {
        ResetState();
        Poolable.OnBringFromPool += ResetState;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_LifeTime > LifeTime)
        {

            NotifyFailed();
        }

        m_LifeTime += Time.deltaTime;

        UpdateSize();
    }

    public void ScorePoints()
    {
        Debug.Log("Dostalismy punkty");

        ParticleSystem particle = SpawnPoint.UntracketSpawn<ParticleSystem>(SuccesParticle, transform.position, transform.rotation);
        particle.Play();
        DeleteObject();
        OnAddScore.Action.Invoke();


    }

    public void NotifyFailed()
    {
        Debug.Log("Przegraliśmy");
        ParticleSystem particle = SpawnPoint.UntracketSpawn<ParticleSystem>(FailParticle, transform.position, transform.rotation);

        particle.Play();
        DeleteObject();
        OnFail.Action.Invoke();
    }


    /// <summary>
    /// Bedzie potrzebne pozniej do object poolera
    /// </summary>
    public void ResetState()
    {
        m_LifeTime = 0.0f;
        UpdateSize();
    }


    /// <summary>
    /// Bedzie potrzebne pozniej do object poolera
    /// </summary>
    void DeleteObject()
    {
        Poolable.PushToPool();
    }

    void UpdateSize()
    {
        transform.localScale = Vector3.one * Mathf.Lerp(MinSize, MaxSize, m_LifeTime / LifeTime);
    }
}
