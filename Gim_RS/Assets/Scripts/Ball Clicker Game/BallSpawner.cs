﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{

    const float SpawnEvery = 0.5f;
    const float MinTimeBetweenSpawnPointUsed = 5.0f;

    public SpawnPoint[] SpawnPoints = new SpawnPoint[0];
    public BallToClick BallPrefab;

    float m_TimeFromLastSpawn;
    

    // Use this for initialization
    void Awake()
    {
        m_TimeFromLastSpawn = SpawnEvery;//aby wymusic spawnowanie na starcie bez dodawania logiki pierwszego rozruchu

        if (SpawnPoints.Length<3)// poniewaz SpawnEvery jest okolo 3 razy mniejszy niz czas zycia kulki
        {
            Debug.LogError("SpawnPoints musi zawierac przynajmniej 3 pola");
            enabled = false;
        }

        if (BallPrefab == null)
        {
            Debug.LogError("BallPrefab nie moze byc nullem");
            enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        m_TimeFromLastSpawn += Time.deltaTime;

        if (m_TimeFromLastSpawn > SpawnEvery)
        { 
            int index = Random.Range(0, SpawnPoints.Length);
            bool hasClearSpawnPoint = false;
            for (int i = 0; i < SpawnPoints.Length; i++)
            {
                index = (index + 1) % SpawnPoints.Length;

                if (SpawnPoints[index].TimeSinceLastSpawn > MinTimeBetweenSpawnPointUsed)
                {
                    hasClearSpawnPoint = true;
                    break;
                }
            }

            if (hasClearSpawnPoint)
            {
                BallToClick ball = SpawnPoints[index].Spawn(BallPrefab);
                ball.ResetState();

                m_TimeFromLastSpawn = 0.0f;
            }
        }
    }
}
