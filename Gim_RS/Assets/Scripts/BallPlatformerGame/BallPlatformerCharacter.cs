﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallPlatformerCharacter : MonoBehaviour
{

    float CurrentHorizontalInput = 0.0f;

    public float MaxSelfPropelledTorque = 2.0f;
    public float Acceleration = 5.0f;
    public float JumpForce = 5.0f;

    [Space]
    public float ChecDistanceMultiplier = 1.1f;
    public float ChecRadiusMultiplier = 0.75f;

    Rigidbody m_RB;
    Rigidbody RB
    {
        get
        {
            if (m_RB == null)
            {
                m_RB = GetComponent<Rigidbody>();
            }
            return m_RB;
        }
    }

    SphereCollider m_SphereCollider;
    SphereCollider SphereCollider
    {
        get
        {
            if (m_SphereCollider == null)
            {
                m_SphereCollider = GetComponent<SphereCollider>();
            }
            return m_SphereCollider;
        }
    }



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        CurrentHorizontalInput = 0.0f;

        if (Input.GetKey(KeyCode.LeftArrow))

        {
            CurrentHorizontalInput = 1.0f;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            CurrentHorizontalInput = -1.0f;
        }

        //we need check ground is we can jumpS
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, SphereCollider.radius * ChecRadiusMultiplier, Vector3.down, out hitInfo, SphereCollider.radius * ChecDistanceMultiplier)) //SphereCollider.radius * GroundChecRadiusMultiplier))
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                RB.AddForce(Vector3.up * JumpForce);
            }

        }
    }

    private void FixedUpdate()
    {
        float currentTorqueMagnitude = RB.angularVelocity.z / MaxSelfPropelledTorque;
        float accelerationMultipier = 1.0f;
        if (Mathf.Sign(CurrentHorizontalInput) == Mathf.Sign(currentTorqueMagnitude))
        {
            accelerationMultipier = (1.0f) - Mathf.Abs(currentTorqueMagnitude);
        }

        RB.AddTorque(Vector3.forward * CurrentHorizontalInput * accelerationMultipier * accelerationMultipier);

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere( transform.position + Vector3.down * SphereCollider.radius * ChecDistanceMultiplier, SphereCollider.radius * ChecRadiusMultiplier);
    }
}
